from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task


@login_required
def task_create(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            receipt = form.save()
            receipt.purchaser = request.user
            receipt.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def task_list(request):
    list = Task.objects.filter(assignee=request.user)
    context = {"list": list}
    return render(request, "tasks/list.html", context)
